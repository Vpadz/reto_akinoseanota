class Agreement < ActiveRecord::Base
	belongs_to :ag, polymorphic: true

	validates :description , presence: true 
end
