class Topic < ActiveRecord::Base
  belongs_to :meeting
  has_many :tasks
  has_many :agreements, as: :ag

  validates_associated :tasks ,:agreements 
end
