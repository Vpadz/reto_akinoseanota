class Meeting < ActiveRecord::Base
	has_and_belongs_to_many :people, join_table: 'meeting_people'
	has_many :topics
	has_many :agreements, as: :ag

	validates :subject,:place, presence: true
	#validates_associated :people, :topics, :agreements
end
