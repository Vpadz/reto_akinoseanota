class Task < ActiveRecord::Base
  belongs_to :topic
  has_and_belongs_to_many :people, join_table: 'person_tasks'
end
