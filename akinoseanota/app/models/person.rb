class Person < ActiveRecord::Base
	has_and_belongs_to_many :meetings, join_table: 'meeting_people'
	has_and_belongs_to_many :tasks, join_table: 'person_tasks'

	validates :name, :last_name,  presence: true
	validates_associated :meetings
end
