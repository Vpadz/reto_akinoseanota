class CreatePersonTask < ActiveRecord::Migration
  def change
    create_table :person_tasks do |t|
      t.references :person, index: true, foreign_key: true
      t.references :task, index: true, foreign_key: true
    end
  end
end
