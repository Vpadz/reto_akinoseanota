class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :description
      t.date :exp_date
      t.string :status
      t.references :topic, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
