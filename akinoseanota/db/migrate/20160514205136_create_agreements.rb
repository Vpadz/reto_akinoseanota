class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.text :description
      t.string :ag_type
      t.integer :ag_id

      t.timestamps null: false
    end
  end
end
