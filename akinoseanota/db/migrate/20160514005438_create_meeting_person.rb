class CreateMeetingPerson < ActiveRecord::Migration
  def change
    create_table :meeting_people do |t|
      t.references :meeting, index: true, foreign_key: true
      t.references :person, index: true, foreign_key: true
    end
  end
end
