class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.time :hour
      t.date :month_day_year
      t.string :place
      t.string :subject
      t.time :duration

      t.timestamps null: false
    end
  end
end
