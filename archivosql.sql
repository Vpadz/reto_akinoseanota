CREATE DATABASE proyecto_db_rails;
CREATE TABLE meetings(
					id serial primary key not null,
					hour time,
					month_day_year date,
					place character varying(20) not null,
					subject character varying(30) not null,
					duration time
);
CREATE TABLE persons(
					id serial primary key not null,
					name character varying(20) not null,
					last_name character varying(20) not null,
					email character varying(20),
					phone_number character varying(20)
);
CREATE TABLE topics(
					id serial primary key not null,
					title character varying(20) not null,
					meeting_id integer references meetings(id)
);
CREATE TABLE tasks(
					id serial primary key not null,
					description text not null,
					exp_date date,
					status character varying(10) not null,
					topic_id integer references topics(id)
);
CREATE TABLE person_task(
					id serial primary key not null,
					person_id integer references persons(id),
					task_id integer references tasks(id)
);
CREATE TABLE meeting_person(
					id serial primary key not null,
					meeting_id integer references meetings(id),
					person_id integer references persons(id)
);
CREATE TABLE Agreement(
					id serial primary key not null,
					description text,
					ag_id integer,
					ag_type character varying(10),
);